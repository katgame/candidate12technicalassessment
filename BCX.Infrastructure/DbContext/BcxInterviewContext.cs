﻿using BCX.Core.Contracts;
using BCX.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace BCX.Infrastructure.DbContext
{
    public partial class BcxInterviewContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public virtual DbSet<Candidate> Candidate { get; set; }
        public virtual DbSet<InterviewSchedule> InterviewSchedule { get; set; }
        private readonly string _connectionString;

        public BcxInterviewContext(IOptions<AppSettings> config)
        {
	        _connectionString = config.Value.ConnectionString;
        }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
	            optionsBuilder.UseSqlServer(_connectionString);
            }
        }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Candidate>(entity =>
			{
				entity.HasIndex(e => e.Id)
					.HasName("UQ__Candidat__3214EC060AD2A005")
					.IsUnique();

				entity.Property(e => e.Id).ValueGeneratedNever();

				entity.Property(e => e.CandidateName)
					.HasMaxLength(50)
					.IsUnicode(false);

				entity.Property(e => e.CandidateSurname)
					.HasMaxLength(50)
					.IsUnicode(false);

				entity.HasOne(d => d.IdNavigation)
					.WithOne(p => p.Candidate)
					.HasForeignKey<Candidate>(d => d.Id)
					.OnDelete(DeleteBehavior.ClientSetNull)
					.HasConstraintName("FK_Candidate_Schedule");
			});

			modelBuilder.Entity<InterviewSchedule>(entity =>
			{
				entity.Property(e => e.Comment)
					.HasMaxLength(500)
					.IsUnicode(false);

				entity.Property(e => e.InterviewDateTime).HasColumnType("date");

				entity.Property(e => e.InterviewerName)
					.HasMaxLength(50)
					.IsUnicode(false);

				entity.Property(e => e.Outcome)
					.HasMaxLength(50)
					.IsUnicode(false);

				entity.Property(e => e.Position)
					.HasMaxLength(50)
					.IsUnicode(false);
			});
		}
	}
}
