﻿using BCX.Core.Contracts;
using BCX.Core.Models;
using BCX.Infrastructure.DbContext;

namespace BCX.Infrastructure.Repository
{
	public class CandidateRepository : EfRepository<Candidate>, ICandidateRepository
	{
		public CandidateRepository(BcxInterviewContext dbContext) : base(dbContext)
		{
		}
	}
}
