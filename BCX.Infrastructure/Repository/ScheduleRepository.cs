﻿using System.Collections.Generic;
using BCX.Core.Contracts;
using BCX.Core.Models;
using BCX.Infrastructure.DbContext;
using Microsoft.EntityFrameworkCore;
using System.Linq;
namespace BCX.Infrastructure.Repository
{
	public class ScheduleRepository : EfRepository<InterviewSchedule>, IScheduleRepository
	{
		public ScheduleRepository(BcxInterviewContext dbContext) : base(dbContext)
		{
		}

		public InterviewSchedule GetScheduleWithCandidateInfo(int Id)
		{
			return _dbContext.InterviewSchedule.Include(x => x.Candidate).SingleOrDefault(y => y.Id == Id);
		}

	}
}
