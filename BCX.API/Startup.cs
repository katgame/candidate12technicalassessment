﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using BCX.API;
using BCX.Core.Contracts;
using BCX.Core.Services;
using BCX.Infrastructure.DbContext;
using BCX.Infrastructure.Repository;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin;
using Swashbuckle.AspNetCore.Swagger;

[assembly: OwinStartup(typeof(Startup))]
namespace BCX.API
{
	
	public class Startup
	{
		public Startup(IHostingEnvironment env)
		{
			var builder = new ConfigurationBuilder().SetBasePath(env.ContentRootPath)
				.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
				.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
				.AddEnvironmentVariables();

			Configuration = builder.Build();
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.Configure<AppSettings>(Configuration.GetSection("appSettings"));
			services.AddAuthorization();
			services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
				.AddJwtBearer(options =>
				{
					options.TokenValidationParameters = new TokenValidationParameters
					{
						ValidateIssuer = true,
						ValidateAudience = true,
						ValidateLifetime = true,
						ValidateIssuerSigningKey = true,
						ValidIssuer = Configuration["Jwt:Issuer"],
						ValidAudience = Configuration["Jwt:Issuer"],
						IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
					};
				});

			services.AddMvc()
				.AddXmlSerializerFormatters();
			services.AddDbContext<BcxInterviewContext>();
			services.AddTransient<IScheduleRepository, ScheduleRepository>();
			services.AddTransient<ICandidateRepository, CandidateRepository>();
			services.AddTransient<IInterviewService, InterviewService>();
			services.AddCors(options =>
			{
				options.AddPolicy("CorsPolicy",
					builder => builder.AllowAnyOrigin()
						.AllowAnyMethod()
						.AllowAnyHeader()
						.AllowCredentials());
			});

			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new Info
				{
					Version = "v1",
					Title = "BCX Interview Scheduling API",
					Description = "This API is responsible for managing interviews for BCX",
					TermsOfService = "None",
					Contact = new Contact
					{
						Name = "bcx",
						Email = "phutichokoe@bcx.co.za",
						Url = "www.bcx.co.za"
					},
					License = new License
					{
						Name = "Use under LICX",
						Url = "www.bcx.co.za"
					}
				});
				// Set the comments path for the Swagger JSON and UI.
				var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
				var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
				c.IncludeXmlComments(xmlPath);
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			app.UseCors("CorsPolicy");
			var value = Configuration["ApplicationSettings"];
			if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

			ConfigureOAuth(app);
			app.UseSwagger();
			app.UseSwaggerUI(c => { c.SwaggerEndpoint("../swagger/v1/swagger.json", "BCX.API"); });
			app.UseAuthentication();
			app.UseMvc();
		}

		public void ConfigureOAuth(IApplicationBuilder app)
		{
		}
	}
}
