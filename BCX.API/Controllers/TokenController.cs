﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BCX.Core.Contracts;
using BCX.Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace BCX.API.Controllers
{
	[Route("api/[controller]")]
	public class TokenController : Controller
	{
		private IConfiguration _config;
		internal ClientTokenDetails validClientDetails { get; set; }
		public TokenController(IOptions<AppSettings> appSettings, IConfiguration config)
		{
			_config = config;
			//Valid Client Id and Key for app settings.
			//Typically this data would be validated from a STS or I'd have to make a DB call with
			//a list of valid client and compare the header info against our data.
			validClientDetails = new ClientTokenDetails
			{
				clientKey = appSettings.Value.ClientIdKeyName,
				clientValue =  Guid.Parse(appSettings.Value.ClientId)
			};
		}
		[AllowAnonymous]
		[HttpPost]
		public IActionResult CreateToken([FromBody]ClientTokenDetails client)
		{
			try
			{
				IActionResult response = Unauthorized();
				if (!ModelState.IsValid)
				{
					return response;
				}

				var userDetails = CheckClient(client);

				if (userDetails)
				{
					var tokenString = BuildToken();
					response = Ok(new { token = tokenString });
				}

				return response;

			}
			catch 
			{

				return BadRequest("Server failed to generate a token. Please contact your administrator");
			}

		}

		/// <summary>
		/// This method would either call a STS to validate the client info or call a database to valid the client Information.
		/// Typically instead of comparing against the app settings value
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		internal bool CheckClient(ClientTokenDetails request)
		{
			return request.clientKey.Contains(validClientDetails.clientKey) && request.clientValue.Equals(validClientDetails.clientValue);
		}

		private string BuildToken()
		{

			var claim = new[]
			{
				new Claim(JwtRegisteredClaimNames.Sub, "BCXInterviewTest"),
				new Claim(JwtRegisteredClaimNames.Email, "phuti.chokoe@bcx.co.za"),
				new Claim(JwtRegisteredClaimNames.UniqueName, "bcx test"),
				new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
			};

			var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
			var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

			var token = new JwtSecurityToken(_config["Jwt:Issuer"],
				_config["Jwt:Issuer"],
				claim,
				expires: DateTime.Now.AddMinutes(30),
				signingCredentials: creds);

			return new JwtSecurityTokenHandler().WriteToken(token);
		}
	}
}