﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BCX.Core.Contracts;
using BCX.Core.DTOs;
using BCX.Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BCX.API.Controllers
{
    [Produces("application/json")]
    public class InterviewSchedulerController : Controller
    {
	    internal IInterviewService _schedulerService { get; }
	    private readonly ILogger logger;
	    private readonly ILogger<InterviewSchedulerController> _logger;

		public InterviewSchedulerController(IInterviewService schedulerService, ILogger<InterviewSchedulerController> Nlogger)
	    {
		    _schedulerService = schedulerService;
		    _logger = Nlogger;
	    }

	    /// <summary>
	    /// Add New Interview Schedule
	    /// </summary>
	    /// <param name="request"></param>
	    /// <returns></returns>
	    [ProducesResponseType(200)]
	    [ProducesResponseType(400)]
	    [ProducesResponseType(500)]
	    [Route("api/createSchedule")]
	    [EnableCors("CorsPolicy")]
	    [HttpPost]
	    [Authorize]
	    public async Task<IActionResult> CreateInterviewSchedule([FromBody] ScheduleRequestDto request)
	    {
		    ScheduleResponseDto result;
		    try
		    {
			    if (!ModelState.IsValid)
			    {
				    return BadRequest(ModelState);
			    }
			    _logger.LogInformation("CreateInterviewSchedule request :" + request);
				result = await _schedulerService.CreateInterviewSchedule(request);
				_logger.LogInformation("CreateInterviewSchedule response :" + result);
				if (result.bSuccess == false)
				    return BadRequest(result);

		    }
		    catch (Exception ex)
		    {
			    _logger.LogInformation("CreateInterviewSchedule exception :" + ex);
				return BadRequest("Error occured with action create schedule");
		    }
		    return new OkObjectResult(result);
	    }

	    /// <summary>
	    /// Returns list of all schedules 
	    /// </summary>
	    /// <param name="request"></param>
	    /// <returns></returns>
	    [ProducesResponseType(200)]
	    [ProducesResponseType(400)]
	    [ProducesResponseType(500)]
	    [Route("api/getAllInterviewSchedules")]
	    [EnableCors("CorsPolicy")]
	    [HttpGet]
	    [Authorize]
	    public async Task<IActionResult> GetAllInterviewSchedules()
	    {
		    IEnumerable<InterviewSchedule> result;
		    try
		    {
			    if (!ModelState.IsValid)
			    {
				    return BadRequest(ModelState);
			    }
			    result = await _schedulerService.GetAllInterviewSchedules();
		    }
		    catch (Exception ex)
		    {
			    _logger.LogInformation("GetAllInterviewSchedules exception :" + ex);
				return BadRequest("Error occured with action get all interview schedules");
		    }
		    return new OkObjectResult(result);
	    }

	    /// <summary>
	    /// Returns list of all schedules 
	    /// </summary>
	    /// <param name="request"></param>
	    /// <returns></returns>
	    [ProducesResponseType(200)]
	    [ProducesResponseType(400)]
	    [ProducesResponseType(500)]
	    [Route("api/getScheduleById/{id}")]
	    [EnableCors("CorsPolicy")]
	    [HttpGet]
	    [Authorize]
	    public async Task<IActionResult> GetInterviewScheduleById(int id)
	    {
		    InterviewSchedule result;
		    try
		    {
			    if (!ModelState.IsValid)
			    {
				    return BadRequest(ModelState);
			    }

			    _logger.LogInformation("GetInterviewScheduleById request id :" + id);
				result = await _schedulerService.GetInterviewScheduleById(id);
			    _logger.LogInformation("GetInterviewScheduleById response :" + result);
			}
		    catch (Exception ex)
		    {
			    _logger.LogInformation("GetInterviewScheduleById exception :" + ex);
				return BadRequest("Error occured with action get interview schedule");
		    }
		    return new OkObjectResult(result);
	    }

		/// <summary>
		/// Get All Candidates
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		[ProducesResponseType(200)]
	    [ProducesResponseType(400)]
	    [ProducesResponseType(500)]
	    [Route("api/getAllCandidates")]
	    [EnableCors("CorsPolicy")]
	    [HttpGet]
	    [Authorize]
	    public async Task<IActionResult> GetAllCandidates()
	    {
		    IEnumerable<Candidate> result;
		    try
		    {
			    if (!ModelState.IsValid)
			    {
				    return BadRequest(ModelState);
			    }

			    result = await _schedulerService.GetAllCandidates();
		    }
		    catch (Exception ex)
		    {
			    _logger.LogInformation("GetAllCandidates exception :" + ex);
				return BadRequest("Error occured with action get all candidates");
		    }
		    return new OkObjectResult(result);
	    }

		/// <summary>
		/// Get Candidate By Name
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		[ProducesResponseType(200)]
		[ProducesResponseType(400)]
		[ProducesResponseType(500)]
		[Route("api/getCandidateByName/{name}")]
		[EnableCors("CorsPolicy")]
		[HttpGet]
		[Authorize]
		public async Task<IActionResult> GetCandidateByName(string name)
		{
			Candidate result;
			try
			{
				if (!ModelState.IsValid)
				{
					return BadRequest(ModelState);
				}
				_logger.LogInformation("GetCandidateByName request name :" + name);
				result = await _schedulerService.GetCandidateByName(name);
				_logger.LogInformation("GetCandidateByName response :" + result);
			}
			catch (Exception ex)
			{
				_logger.LogInformation("GetCandidateByName exception :" + ex);
				return BadRequest("Error occured with action get candidate");
			}
			return new OkObjectResult(result);
		}

		/// <summary>
		/// Remove Schedule by passing a schedule Id
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		[ProducesResponseType(200)]
		[ProducesResponseType(400)]
		[ProducesResponseType(500)]
		[Route("api/deleteSchedule/{scheduleId}")]
		[EnableCors("CorsPolicy")]
		[HttpDelete]
		[Authorize]
		public async Task<IActionResult> DeleteInterviewAppoint(int scheduleId)
		{
			bool result;
			try
			{
				if (!ModelState.IsValid)
				{
					return BadRequest(ModelState);
				}
				_logger.LogInformation("DeleteInterviewAppoint request scheduleId :" + scheduleId);
				result = await _schedulerService.DeleteInterviewAppoint(scheduleId);
				_logger.LogInformation("DeleteInterviewAppoint response :" + result);
			}
			catch (Exception ex)
			{
				_logger.LogInformation("DeleteInterviewAppoint Exception :" + ex);
				return BadRequest("Error occured with action get candidate");
			}
			return new OkObjectResult(result);
		}

		/// <summary>
		/// Update Schedule 
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		[ProducesResponseType(200)]
		[ProducesResponseType(400)]
		[ProducesResponseType(500)]
		[Route("api/updateSchedule")]
		[EnableCors("CorsPolicy")]
		[HttpPut]
		[Authorize]
		public async Task<IActionResult> AmendInterviewSchedule([FromBody] AmendScheduleRequestDto request)
		{
			AmendScheduleResponseDto result;
			try
			{
				if (!ModelState.IsValid)
				{
					return BadRequest(ModelState);
				}
				_logger.LogInformation("AmendInterviewSchedule request :" + request);
				result = await _schedulerService.AmendInterviewSchedule(request);
				_logger.LogInformation("AmendInterviewSchedule response :" + result);
				if (result.bSuccess == false)
					return BadRequest(result);
			}
			catch (Exception ex)
			{
				_logger.LogInformation("AmendInterviewSchedule Exception :" + ex);
				return BadRequest("Error occured with action amend schedule");
			}
			return new OkObjectResult(result);
		}
	}
}