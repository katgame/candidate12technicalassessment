﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BCX.Core.Contracts
{
	public interface ISpecification<T>
	{
		Expression<Func<T, bool>> Criteria { get; }
		List<Expression<Func<T, object>>> Includes { get; }
		List<string> IncludeStrings { get; }
	}
}
