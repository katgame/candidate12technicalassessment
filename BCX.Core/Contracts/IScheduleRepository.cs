﻿using System;
using System.Collections.Generic;
using System.Text;
using BCX.Core.Models;

namespace BCX.Core.Contracts
{
	public interface IScheduleRepository : IRepository<InterviewSchedule>, IAsyncRepository<InterviewSchedule>
	{
		InterviewSchedule GetScheduleWithCandidateInfo(int Id);
	}
}
