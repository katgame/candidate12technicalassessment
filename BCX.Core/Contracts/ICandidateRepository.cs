﻿using System;
using System.Collections.Generic;
using System.Text;
using BCX.Core.Models;

namespace BCX.Core.Contracts
{
	public interface ICandidateRepository : IRepository<Candidate>, IAsyncRepository<Candidate>
	{
	}
}
