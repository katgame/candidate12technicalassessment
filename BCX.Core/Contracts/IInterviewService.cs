﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BCX.Core.DTOs;
using BCX.Core.Models;

namespace BCX.Core.Contracts
{
	public interface IInterviewService
	{
		Task<ScheduleResponseDto> CreateInterviewSchedule(ScheduleRequestDto request);
		Task<IEnumerable<InterviewSchedule>> GetAllInterviewSchedules();
		Task<InterviewSchedule> GetInterviewScheduleById(int id);
		Task<IEnumerable<Candidate>> GetAllCandidates();
		Task<Candidate> GetCandidateByName(string name);
		Task<bool> DeleteInterviewAppoint(int id);
		Task<AmendScheduleResponseDto> AmendInterviewSchedule(AmendScheduleRequestDto request);
	}
}
