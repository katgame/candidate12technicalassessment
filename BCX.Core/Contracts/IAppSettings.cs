﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BCX.Core.Contracts
{
	public interface IAppSettings
	{
		string ConnectionString { get; set; }
		string ClientId { get; set; }
		string ClientIdKeyName { get; set; }

	}
	public class AppSettings : IAppSettings
	{
		public string ConnectionString { get; set; }
		public string ClientId { get; set; }
		public string ClientIdKeyName { get; set; }

	}
}
