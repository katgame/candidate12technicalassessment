﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BCX.Core.Entities
{
	public class BaseEntity
	{
		public int Id { get; set; }
	}
}
