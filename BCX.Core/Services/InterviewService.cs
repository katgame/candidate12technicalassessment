﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BCX.Core.Contracts;
using BCX.Core.Models;
using System.Linq;
using BCX.Core.DTOs;

namespace BCX.Core.Services
{
	public class InterviewService : IInterviewService
	{
		internal ICandidateRepository _candidateRepo;
		internal IScheduleRepository _scheduleRepository;

		public InterviewService(ICandidateRepository candidateRepo, IScheduleRepository scheduleRepository)
		{
			_candidateRepo = candidateRepo;
			_scheduleRepository = scheduleRepository;
		}
		public async Task<ScheduleResponseDto> CreateInterviewSchedule(ScheduleRequestDto request)
		{
			try
			{
				var schedule = new InterviewSchedule
				{
					InterviewDateTime = request.InterviewDateTime,
					Position = request.Position,
					InterviewerName = request.InterviewerName,
					Comment = request.Comment,
					Outcome = request.Outcome,
					Candidate = new Candidate
					{
						CandidateName = request.CandidateName,
						CandidateSurname = request.CandidateSurname
					}
				};
				var scheduleResponse = _scheduleRepository.Add(schedule);

				return new ScheduleResponseDto
				{
					ScheduleId = scheduleResponse.Id,
					bSuccess = true,
					Errors = null
				};

			}
			catch (Exception e)
			{
				return new ScheduleResponseDto
				{
					bSuccess = false,
					Errors = e.Message
				};
			}
		}

		public async Task<IEnumerable<InterviewSchedule>> GetAllInterviewSchedules()
		{
			return _scheduleRepository.ListAll();
		}

		public async Task<InterviewSchedule> GetInterviewScheduleById(int id)
		{
			return _scheduleRepository.GetById(id);
		}

		public async Task<IEnumerable<Candidate>> GetAllCandidates()
		{
			return _candidateRepo.ListAll();
		}

		public async Task<Candidate> GetCandidateByName(string name)
		{
			return _candidateRepo.ListAllAsync().Result.SingleOrDefault(x => x.CandidateName != null && x.CandidateName.Contains(name));
		}

		public async Task<bool> DeleteInterviewAppoint(int scheduleId)
		{
			try
			{
				var scheduleInfo = _scheduleRepository.GetScheduleWithCandidateInfo(scheduleId) ?? throw new ArgumentNullException("_scheduleRepository.GetById(scheduleId)");
				_candidateRepo.Delete(scheduleInfo.Candidate);
				_scheduleRepository.Delete(scheduleInfo);
				return true;
			}
			catch
			{
				return false;
			}
		}

		public async Task<AmendScheduleResponseDto> AmendInterviewSchedule(AmendScheduleRequestDto scheduleDetails)
		{
			try
			{
				var response = new AmendScheduleResponseDto();
				var currentEntity = _scheduleRepository.GetById(scheduleDetails.Id);
				if (currentEntity != null)
				{
					currentEntity.Comment = scheduleDetails.Comment;
					currentEntity.Outcome = scheduleDetails.Outcome;
					currentEntity.InterviewerName = scheduleDetails.InterviewerName;
					currentEntity.Position = scheduleDetails.Position;
					currentEntity.InterviewDateTime = scheduleDetails.InterviewDateTime;
				}
				else throw new Exception("Schedule Information could not be found");
				

				await _scheduleRepository.UpdateAsync(currentEntity);

				response.AmendedInformation = scheduleDetails;
				response.bSuccess = true;
				response.Errors = null;
				return response;
			}
			catch (Exception e)
			{

				return new AmendScheduleResponseDto
				{
					bSuccess = false,
					Errors = e.Message
				};
			}
		}

	}
}
