﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BCX.Core.DTOs
{
	[DataContract]
	public class ScheduleRequestDto 
	{

		/// <summary>
		/// Candidates Name for Interview
		/// </summary>
		[DataMember(Name = "CandidateName")]
		public string CandidateName { get; set; }

		/// <summary>
		/// Candidates Surname for Interview
		/// </summary>
		[DataMember(Name = "CandidateSurname")]
		public string CandidateSurname { get; set; }

		/// <summary>
		/// Position Description
		/// </summary>
		[DataMember(Name = "Position")]
		public string Position { get; set; }

		/// <summary>
		/// Name of Individual Conducting Interview
		/// </summary>
		[DataMember(Name = "InterviewerName")]
		public string InterviewerName { get; set; }

		/// <summary>
		/// Interview Date and Time
		/// </summary>
		[DataMember(Name = "InterviewDateTime")]
		public DateTime InterviewDateTime { get; set; }

		/// <summary>
		/// Interview Comments
		/// </summary>
		[DataMember(Name = "Comment")]
		public string Comment { get; set; }

		/// <summary>
		/// Interview Outcome
		/// </summary>
		[DataMember(Name = "Outcome")]
		public string Outcome { get; set; }

	}
}
