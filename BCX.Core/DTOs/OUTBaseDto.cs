﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BCX.Core.DTOs
{
	[DataContract(Namespace = "")]
	public class OUTBaseDto
	{
		[DataMember]
		public bool bSuccess { get; set; }

		[DataMember]
		public string Errors { get; set; }
	}
}
