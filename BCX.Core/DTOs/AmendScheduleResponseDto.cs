﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BCX.Core.DTOs
{
	public class AmendScheduleResponseDto : OUTBaseDto
	{
		/// <summary>
		/// Previous Information
		/// </summary>
		[DataMember(Name = "PreviousInformation")]
		public dynamic PreviousInformation { get; set; }

		/// <summary>
		/// Amended Information
		/// </summary>
		[DataMember(Name = "AmendedInformation")]
		public dynamic AmendedInformation { get; set; }
	}
}
