﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BCX.Core.DTOs
{

	[DataContract]
	public class ScheduleResponseDto : OUTBaseDto
	{
		/// <summary>
		/// Interview Confirmation Id
		/// </summary>
		[DataMember(Name = "ScheduleId")]
		public int ScheduleId { get; set; }
	}
}
