﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BCX.Core.Models
{
	public class ClientTokenDetails
	{
		public string clientKey { get; set; }
		public Guid clientValue { get; set; }
	}
}
