﻿using System;
using System.Collections.Generic;
using BCX.Core.Contracts;
using BCX.Core.Entities;

namespace BCX.Core.Models
{
    public partial class Candidate : BaseEntity, IAggregateRoot
	{
		public string CandidateName { get; set; }
		public string CandidateSurname { get; set; }

		public InterviewSchedule IdNavigation { get; set; }
	}
}
