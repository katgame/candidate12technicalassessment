﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BCX.Core.Contracts;
using BCX.Core.Entities;
using BCX.Core.Models;

namespace BCX.Core.Models
{
	
	public partial class InterviewSchedule : BaseEntity, IAggregateRoot
	{
		public string Position { get; set; }
		public string InterviewerName { get; set; }
		public DateTime? InterviewDateTime { get; set; }
		public string Comment { get; set; }
		public string Outcome { get; set; }

		public Candidate Candidate { get; set; }
	}
}
