import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {
  /**
   * Encrypts and stores user data
   *
   * @param {string} key
   * @param {*} authData
   * @memberof LocalStorageService
   */
  setLocalAuthData(key: string, authData: any): void {
    localStorage.setItem(key, window.btoa(JSON.stringify(authData)));
  }

  /**
   * Decrypts and retrieves user data
   *
   * @param {string} key
   * @returns {*}
   * @memberof LocalStorageService
   */
  getLocalAuthData(key: string): any {
    if (key && localStorage.getItem(key)) {
      return JSON.parse(window.atob(localStorage.getItem(key)));
    }
  }

  /**
   * Removes the stored user data
   *
   * @param {string} key
   * @memberof LocalStorageService
   */
  removeLocalAuthData(key: string) {
    localStorage.removeItem(key);
  }
}
