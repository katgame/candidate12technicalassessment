import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ErrorHandlerComponent } from 'src/app/shared/error-handler/error-handler.component';
import * as SpinnerActions from '../../state/shared/actions/spinner.action';
import { Store } from '@ngrx/store';
import { AppState } from 'src/state/app.interfaces';


@Injectable()
export class ErrorDialogService {

    constructor(
        public dialog: MatDialog,
        private store: Store<AppState>,
    ) {
    }
    openDialog(data): void {
        this.store.dispatch(new SpinnerActions.HideSpinner());

        const dialogConfig: MatDialogConfig = {
            maxWidth: '80vw',
            data: data,
            disableClose: true
        };
        this.dialog.open(ErrorHandlerComponent, dialogConfig);
    }
}
