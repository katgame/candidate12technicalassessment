import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError, mergeMap } from 'rxjs/operators';
import { ErrorDialogService } from './error-dialog.service';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { Token } from '@angular/compiler';

@Injectable()
export class HeaderInterceptor implements HttpInterceptor {

  constructor(private injector: Injector,
    public errorDialogService: ErrorDialogService,
    private authService: AuthService,
    private localStgSvc: LocalStorageService) {

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if ( !request.url.includes('assets')) {
      request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
      request = request.clone({ headers: request.headers.set('content-type', 'application/json') });
    }

    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        // if (event instanceof HttpResponse) {
        //     console.log('event--->>>', event);
        // }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        if (!error.url.includes('Token.Api')) {
          let errorResponse = {};
          // Catch all error's except bad request(s)
          switch (error.status) {
            case 400:
              errorResponse = {
                message: error.error.errors,
                status: error.statusText
              }
             this.errorDialogService.openDialog(errorResponse);

              break;
            case 401:
                errorResponse = {
                  message: 'Unathorized server call - please contact your administrator',
                  status: 'Oops - Server error'//error.status
                };
            this.errorDialogService.openDialog(errorResponse);
            //  this.authService.logout();
              break;
            // case 503:
            //   // TODO navigate to error page
            //   break;
            // case 500:
            //   debugger;
            //   break;
            default:
              errorResponse = {
                message: error && error.error.reason ? error.error.reason : error.error,
                status: 'Oops - Server error'//error.status
              };
              this.errorDialogService.openDialog(errorResponse);
              break;
          }
        }
        return throwError(error);
      }));
  }

}
