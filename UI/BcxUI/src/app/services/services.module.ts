import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocalStorageService } from './local-storage.service';
import { AuthService } from './auth.service';
import { AuthGuardService } from './auth-guard.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [ LocalStorageService, AuthGuardService, AuthService, JwtHelperService]
})
export class ServicesModule { }


 