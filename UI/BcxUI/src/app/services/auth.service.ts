import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {Observable, pipe, throwError} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import decode from 'jwt-decode';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private message: string;
  token: string;
  constructor(private _router: Router) {
    this.token = environment.JwtToken;

   }

  clear(): void {
    localStorage.clear();
  }

  isAuthenticated(): boolean {
    return localStorage.getItem('token') != null && !this.isTokenExpired();
  }

  isTokenExpired(): boolean {
    return false;
  }

  login(): void {
    localStorage.setItem('token', this.token);
    this._router.navigate(['/view']);
  }

  logout(): void {
    this.clear();
    this._router.navigate(['/home']);
  }

  decode() {
    return decode(localStorage.getItem('token'));
  }
}
