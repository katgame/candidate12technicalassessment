
export class User {

    constructor() {
        this.firstName = '';
        this.lastName = '';
        this.workNumber = '';
        this.cellNumber = '';
        this.emailAddress = '';
        this.password = '';
        this.confirmPassword = '';
    }
    firstName: string;
    lastName: string;
    workNumber: string;
    cellNumber: string;
    emailAddress: string;
    password: string;
    confirmPassword: string;
}
