export interface DialogData {
    title: string;
    message: string;
    ShowButton1: boolean;
    ShowButton2: boolean;
    button1Caption: string;
    button2Caption: string;
  }
