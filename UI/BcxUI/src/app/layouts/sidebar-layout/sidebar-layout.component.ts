import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { AuthUserInfo } from 'src/app/auth/models/auth-user.model';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { Subscription } from 'rxjs';
import { AuthTokenInfo } from 'src/app/auth/models/auth-token.model';
import { ClientDetails } from 'src/app/relativity/models/models/clientDetails';
import { environment } from 'src/environments/environment';
import * as JWT from 'jwt-decode';

@Component({
  selector: 'app-sidebar-layout',
  templateUrl: './sidebar-layout.component.html',
  styleUrls: ['./sidebar-layout.component.css']
})
export class SidebarLayoutComponent implements OnInit {
  isExpanded = false;
  isAuthorizedSubscription: Subscription;
  isAuthorized: boolean;
  isAdmin = false;
  client: ClientDetails;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  profile: AuthUserInfo;
  constructor(
    private authService: AuthService,
    private breakpointObserver: BreakpointObserver,
  ) {  }

  ngOnInit() {

    }

    login() {
      //this.oidcSecurityService.authorize();
    }

  refreshSession() {
    //  this.oidcSecurityService.authorize();
    }

  ngOnDestroy(): void {
    //  this.isAuthorizedSubscription.unsubscribe();
    }
  signOut() {
    this.authService.logout();
    }
}
