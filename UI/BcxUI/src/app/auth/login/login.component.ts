import { Component, OnInit, OnDestroy } from '@angular/core';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription, Observable } from 'rxjs';
import { LoginModel } from 'src/app/models/login-model';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { User } from 'src/app/models/user-details';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  constructor(private router: Router,
    public modal: NgbModal,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private authService: AuthService
  ) {  this.DBpath = environment.UserDatabase; }

  isExpanded = false;
  isAuthorizedSubscription: Subscription;
  isAuthorized: boolean;
  errorMessage: string;
  userFormGroup: FormGroup;
  DBpath: string;
  submitted = false;
  loginDetails = new LoginModel();
  public loading = false;
  userList: User[] = [];

  get f1() { return this.userFormGroup.controls; }

  getUserDetails(): Observable<User[]> {
    return this.http.get<User[]>(this.DBpath);
  }


  setFormValidations() {
    this.userFormGroup = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
  ngOnInit() {
    document.body.classList.add('auth-background');
  }
  ngOnDestroy() {
    document.body.classList.remove('auth-background');
  }
  login() {
    if (!this.loginDetails.password || !this.loginDetails.username) {
      this.errorMessage = 'Please provide valid details';
      return;
    }
    this.getUserDetails().subscribe(response => {
      this.userList = response;
      // tslint:disable-next-line:max-line-length
      const validUser = this.userList.filter(t => t.emailAddress === this.loginDetails.username && t.password === this.loginDetails.password);
      if (validUser.length > 0) {
        this.authService.login();
      } else {
        this.errorMessage = 'Login was unsuccessful';
      }
    },
      response => {
        this.errorMessage = 'Server error occured please contact your admin';
        this.spinner.hide();
      },
      () => {
        this.spinner.hide();
      });
    }

    register() {
      this.router.navigateByUrl('/userRegistration');
    }

    refreshSession() {

    }
    logout() {
      this.authService.logout();
    }
  }
