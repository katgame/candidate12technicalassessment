export class AuthTokenInfo {
	public aud: string;
	public iss: string;
	public iat: number;
	public nbf: number;
	public exp: number;
	public aio: string;
	public name: string;
	public nonce: string;
	public oid: string;
	public preferred_username: string;
	public sub: string;
	public tid: string;
	public uti: string;
	public ver: string;
}
