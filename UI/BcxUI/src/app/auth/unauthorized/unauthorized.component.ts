import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'unauthorized',
  templateUrl: './unauthorized.component.html',
  styleUrls: ['./unauthorized.component.css']
})
export class UnauthorizedComponent implements OnInit, OnDestroy {

  constructor() { }

  ngOnInit() {
    document.body.classList.add('auth-background');    
  }

  ngOnDestroy(){
    document.body.classList.remove('auth-background');
  }

}
