import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../shared/material-module';
import { LoginErrorComponent } from './dialogs/login-error/login-error.component';
import { authRoutes } from './auth-routing.module';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
@NgModule({
  declarations: [ LoginErrorComponent, UnauthorizedComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    RouterModule.forChild(authRoutes)
  ],
  entryComponents: [LoginErrorComponent]
})
export class LocalAuthModule {
  constructor(
  ) {
    console.log('Auth module loading');
  }
}