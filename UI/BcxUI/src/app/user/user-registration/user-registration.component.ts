import { User } from 'src/app/models/user-details';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

// tslint:disable-next-line:max-line-length
import { MatDialog, MatDialogRef, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { DialogData } from '../../relativity/models/models/DialogData';
import { Router } from '@angular/router';
import { DialogBoxComponent } from 'src/app/dialogs/default-dialog/dialog-box.component';
import * as saveAs from 'file-saver';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { finished } from 'stream';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {

  userdetails = new User();
  buttonCaption = 'Register';
  submitted = false;
  Updating = false;
  userList: User[] = [];
  DBpath: string;
  dialogdata: DialogData;
  userFormGroup: FormGroup;
  action = true;
  setAutoHide = true;
  autoHide = 3000;
  message: string;
  actionButtonLabel: string;
  valueRelativity = false;
  horizontalPosition: MatSnackBarHorizontalPosition = 'right';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  addExtraClass = false;
  dialogBox: MatDialogRef<DialogBoxComponent, any>;
  constructor(private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private dialog: MatDialog,
    private router: Router,
    private http: HttpClient,
    public snackBar: MatSnackBar) {
    this.DBpath = environment.UserDatabase;
  }

  get f1() { return this.userFormGroup.controls; }

  getUserDetails(): Observable<User[]> {
    return this.http.get<User[]>(this.DBpath);
  }

  // onNoClick(): void {
  //   this.dialogRef.close();
  // }

  passwordsMatch() {
    return this.userdetails.password === this.userdetails.confirmPassword;
  }

  ngOnInit() {
    this.setFormValidations();
    this.getUserDetails().subscribe(response => {
      this.userList = response;
      console.log(response);
    },
      response => {
        this.spinner.hide();
        this.dialogdata = this.CreateDialog('Close', '',
          'User Details could not be loaded', false, true, 'Database Response Error');
        this.openDialog(this.dialogdata);
      },
      () => {
        this.spinner.hide();
      });
  }

  

  setFormValidations() {
    this.userFormGroup = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      workNumber: ['', Validators.required],
      cellNumber: ['', Validators.required],
      emailAddress: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }

  clear() {
    this.userdetails.password = '';
    this.userdetails.cellNumber = '';
    this.userdetails.confirmPassword = '';
    this.userdetails.emailAddress = '';
    this.userdetails.firstName = '';
    this.userdetails.lastName = '';
  }

  addUser() {
    this.submitted = true;

    if (this.userFormGroup.invalid || !this.passwordsMatch) {
      return;
    }
    this.AddUserDetailsToFile();
  }


  AddUserDetailsToFile() {
    this.userList.push(this.userdetails);
    const jsonData = JSON.stringify(this.userList, null, 2);
    const jsonfile = require('jsonfile');
    const path = this.DBpath;
    jsonfile.writeFile(path, jsonData, { flag: 'a' }).subscribe(response => {
      this.spinner.hide();
      this.message = 'You have registered successfully';
      this.actionButtonLabel = 'Success';
     // this.dialogRef.close();
      this.open();
      this.spinner.hide();
    },
      response => {
        this.message = 'Error Registering your profile';
        this.actionButtonLabel = 'Unsuccessful';
      //  this.dialogRef.close();
        this.open();
        this.spinner.hide();
      },
      () => {
        this.spinner.hide();
      }
    );
  }

  open() {
    const config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 2;
    this.snackBar.open(this.message, this.action ? this.actionButtonLabel : undefined, config);
  }

  openDialog(data: DialogData): void {
    this.dialogBox = this.dialog.open(DialogBoxComponent, {
      width: '550px', data
    });
  }

  CreateDialog(button1Caption: string, button2Caption: string,
    message: string, ShowButton1: boolean, ShowButton2: boolean, title: string): DialogData {
    return {
      button1Caption,
      button2Caption,
      message,
      ShowButton1,
      ShowButton2,
      title
    };
  }
}
