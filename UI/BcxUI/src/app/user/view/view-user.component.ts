
import { Component, OnInit } from '@angular/core';
import { FormGroup , Validators, FormControl , FormBuilder} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { DialogBoxComponent } from 'src/app/dialogs/default-dialog/dialog-box.component';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/models/user-details';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css']
})
export class ViewUserComponent implements OnInit {
   userList: User[] = [];
   submitted = false;

   userFormGroup: FormGroup;
   dialogBox: MatDialogRef<DialogBoxComponent, any>;
   DBpath: string;
   constructor(private formBuilder: FormBuilder,
    private router: Router,
    private spinner: NgxSpinnerService,
    private dialog: MatDialog,
    private http: HttpClient) { this.DBpath = environment.UserDatabase; }

    get f1() { return this.userFormGroup.controls; }
    ngOnInit() {
     this.getUserDetails().subscribe(response => {
      this.userList = response;
      console.log(response);
    },
      response => {
        this.spinner.hide();
      },
      () => {
        this.spinner.hide();
      });
  }

  getUserDetails(): Observable<User[]> {
     return this.http.get<User[]>(this.DBpath);
  }
}
