import {Component, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

export interface DialogData {
    title: string;
    message: string;
    ShowButton1: boolean;
    ShowButton2: boolean;
    button1Caption: string;
    button2Caption: string;
  }

@Component({
templateUrl:  'dialog-box.component.html'
})
export  class  DialogBoxComponent {
    constructor(private  dialogRef:  MatDialogRef<DialogBoxComponent>,  @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    }
    onNoClick(): void {
        this.dialogRef.close();
      }
}
