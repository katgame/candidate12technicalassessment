import { ActionReducerMap, MetaReducer } from "@ngrx/store";
import { environment } from "src/environments/environment";
import * as fromSpinner from './../../state/shared/reducers/spinner.reducer';
import * as fromSnackbar from './../../state/shared/reducers/snackbar.reducer';

export interface AppState {
}

export const reducers: ActionReducerMap<AppState> = {
    spinner: fromSpinner.reducer,
    snackbar: fromSnackbar.reducer
};

export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];