import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { AuthTokenInfo } from '../auth/models/auth-token.model';
import { ClientDetails } from '../relativity/models/models/clientDetails';
import { environment } from 'src/environments/environment';
import * as JWT from 'jwt-decode';
import { LocalStorageService } from '../services/local-storage.service';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {
  isExpanded = false;
  isAuthorizedSubscription: Subscription;
  isAuthorized: boolean;
  client: ClientDetails;

  constructor(    private authService: AuthService,
          public router: Router) {
    this.client = new ClientDetails();
  }

  ngOnInit() {  }

  ngOnDestroy(): void {
    this.authService.logout();
  }

  login() {
    this.authService.login();
  }

  refreshSession() {
    this.authService.isAuthenticated();
  }

  logout() {
    this.authService.login();
  }
  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
}
