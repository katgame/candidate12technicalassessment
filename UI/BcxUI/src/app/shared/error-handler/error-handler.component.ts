import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'error-handler',
  templateUrl: './error-handler.component.html',
  styleUrls: ['./error-handler.component.css']
})
export class ErrorHandlerComponent implements OnInit {  
  error: any;
  constructor(
    // @Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ErrorHandlerComponent>
  ) {
    this.error = data;
  }

  ngOnInit() {
  }

}
