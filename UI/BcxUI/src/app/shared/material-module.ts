import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatNativeDateModule, MatIconModule, MatSidenavModule, MatListModule, MatToolbarModule,
  MatCardModule, MatInputModule, MatTableModule, MatPaginatorModule, MatSortModule, MatDialogModule, MatExpansionModule,
  MatMenuModule, MatSlideToggleModule, MatTabsModule, MatFormFieldModule,
   MatSelectModule, MatStepperModule, MatDatepickerModule, MatProgressSpinnerModule,
    MatProgressBarModule, MatBottomSheetModule, MatCheckboxModule, MatRippleModule,
     MatTooltipModule, MatRadioModule, MatAutocompleteModule } from '@angular/material';

@NgModule({
  imports: [CommonModule, MatButtonModule, MatToolbarModule, MatNativeDateModule, MatIconModule,
    MatSidenavModule, MatListModule, MatCardModule, MatInputModule, MatTableModule, MatPaginatorModule,
    MatSortModule, MatDialogModule, MatExpansionModule, MatMenuModule, MatSlideToggleModule,
    MatTabsModule, MatFormFieldModule, MatSelectModule, MatStepperModule, MatDatepickerModule,
    MatProgressSpinnerModule, MatProgressBarModule, MatBottomSheetModule, MatCheckboxModule, MatRippleModule,
     MatTooltipModule, MatRadioModule, MatAutocompleteModule],
  exports: [CommonModule, MatButtonModule, MatToolbarModule, MatNativeDateModule, MatIconModule,
    MatSidenavModule, MatListModule, MatCardModule, MatInputModule, MatTableModule, MatPaginatorModule,
    MatSortModule, MatDialogModule, MatExpansionModule, MatMenuModule, MatSlideToggleModule, MatTabsModule,
    MatFormFieldModule, MatSelectModule, MatStepperModule, MatDatepickerModule, MatProgressSpinnerModule,
    MatProgressBarModule, MatBottomSheetModule, MatCheckboxModule, MatRippleModule, MatTooltipModule,
     MatRadioModule, MatAutocompleteModule],
})
export class MaterialModule { }
 