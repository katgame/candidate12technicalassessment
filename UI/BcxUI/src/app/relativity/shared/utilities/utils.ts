/**
 * Groups item properties by category and sets default(s) for given control type(s)
 *
 * @export
 * @param {*} objectArray
 * @param {*} property
 * @returns
 */
export function groupPropsByCategory(objectArray, property) {
  // Get Categories for display fields only  
  // return objectArray.reduce(function (acc, obj) {
  return objectArray.filter(x => x.isDisplay).reduce(function (acc, obj) {
    var key = (obj[property] == null) ? 'Config missing' : ((obj[property]) == '' ? 'Config missing' : obj[property]);
    if (!acc[key]) {
      acc[key] = [];
    }

    switch (obj.controlType) {
      case "date":
        // obj.value = new Date();        
        obj.value = (obj.value != null) ? new Date(obj.value) : new Date();
        break;
      case "checkbox":
        obj.value = (obj.value) ? ((typeof obj.value === 'string') ? JSON.parse(obj.value.toLowerCase()) : JSON.parse(obj.default)) : false;
        break;
      case "select":
        // debugger;
        obj.lookUpValue = (typeof obj.lookUpValue === 'string') ? JSON.parse(obj.lookUpValue) : obj.lookUpValue;
        // obj.value = (obj.default != '') ? obj.default : obj.value;
        obj.value = (!obj.value) ? obj.default : obj.value;
        break;
      case "addressLookup":
        obj.value = obj.value;
        break;
      case "personLookup":
        obj.value = obj.value;
        break;
      case "number":
        // obj.value = (obj.value === "" || obj.value === null) ? 0 : obj.value;
        obj.value = (obj.value === "" || obj.value === null) ? obj.default : obj.value;
        break;
      case "text":
        obj.value = obj.value;
        break;
      case "vehicleYear":
        obj.value = (!obj.value) ? obj.default : obj.value;
        break;
      case "vehicleMake":
        obj.value = (!obj.value) ? obj.default : obj.value;
        break;
      case "vehicleModel":
        obj.value = (!obj.value) ? obj.default : obj.value;
        break;
      default:
        obj.value = obj.default;
        break;
    }

    acc[key].push(obj);
    return acc;
  }, {});
}

/**
 * Returns unique item(s) from Array of strings
 *
 * @export
 * @param {*} value
 * @param {*} index
 * @param {*} self
 * @returns
 */
export function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}

/**
 * Generates and returns a new GUID
 */
export function newGuid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

export function getItemTypeIcon(itemType: string) {
  return `assets/icon/${itemType.toLowerCase()}.svg`;
}

export function getSum(total, num) {
  debugger;
  return total + num;
}
