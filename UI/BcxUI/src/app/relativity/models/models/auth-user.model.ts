export class AuthUserInfo {
    public sub: string;
    public role: Array<string>;
    public scope: Array<string>;
    public given_name: string;
    public email: string;
}
