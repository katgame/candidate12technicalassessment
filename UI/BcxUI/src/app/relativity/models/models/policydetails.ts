
    export interface PolicyDetails {
        ClaimItem: ClaimItem[];
        PolicyNumber: string;
        ClaimStatus: string;
        ClaimKey: string;
        ClearanceCode: string;
        Excess: string;
        AdditionalExcess: string;
        ReinsuranceExcess: string;
        VolExcess: string;
        TotalExcess: string;
        TimeStamp: Date;
    }

    export interface Coordinates {
        Longitude: string;
        Latitude: string;
    }

    export interface ContactDetails {
        Email: string;
        CellPhone: string;
    }

    export interface PoliceDetails {
        CaseNumber: string;
        Date: Date;
        PoliceStation: string;
        InvestigationOfficer: string;
        ContactDetails: ContactDetails;
    }

    export interface ContactDetails2 {
        Email: string;
        CellPhone: string;
    }

    export interface Person {
        Name: string;
        IdentityNumber: string;
        LicenceObtainDate: Date;
        LicenceNumber: string;
        ConsentFromPolicyHolder: string;
        ContactDetails: ContactDetails2;
        Status: string;
        PersonType: string;
        PersonId: string;
        Surname: string;
        LicenseCode: string;
    }

    export interface DamagedItem {
        Id: string;
        Name: string;
        Description: string;
    }

    export interface VehicleDetails {
        MMCode: string;
        Make: string;
        Model: string;
        Year: string;
        Color: string;
        RegistrationNo: string;
    }

    export interface ThirdPartyDetail {
        VehicleDetails: VehicleDetails;
        InsurerName: string;
        PersonId: string;
        MobileNumber: string;
        EmailAddress: string;
        ThirdParyHasInsurance: boolean;
    }

    export interface Address {
        PostalCode: string;
        Province: string;
        Line1: string;
        Line2: string;
        Line3: string;
        Line4: string;
    }

    export interface ClaimItem {
        Coordinates: Coordinates;
        PoliceDetails: PoliceDetails;
        Persons: Person[];
        DamagedItems: DamagedItem[];
        ThirdPartyDetails: ThirdPartyDetail[];
        ClaimItemId: string;
        Note: string;
        TrackingConsent: boolean;
        TrackingDevice: boolean;
        TrackingDeviceCompany: string;
        RequiredServices: string[];
        CauseCode: string;
        PerilCode: string;
        WhereFrom: string;
        WhereTO: string;
        Address: Address;
        ItemType: string;
        ItemStatus: string;
        DateAndTime: Date;
        TimeOfLoss: Date;
        IncidentDateAndTime: Date;
    }



