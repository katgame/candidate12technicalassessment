
// tslint:disable-next-line:class-name
export class relativityLookUp {

    constructor() {
        this.id = '';
        this.relativityId = '';
        this.relativityKey = '';
        this.relativityValue = '';
    }
    id: string;
    relativityId: string;
    relativityKey: string;
    relativityValue: string;
}
