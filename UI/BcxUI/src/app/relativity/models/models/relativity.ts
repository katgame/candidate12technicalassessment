
// tslint:disable-next-line:class-name
export class relativity {

    constructor() {
        this.id = '';
        this.name = '';
        this.description = '';
        this.active = true;
    }
    id: string;
    name: string;
    description: string;
    active: boolean;
}
