
export class Assessor {

    constructor() {
        this.id = '';
        this.companyName = '';
        this.companyRegNo = '';
        this.firstName = '';
        this.lastName = '';
        this.workNumber = '';
        this.cellNumber = '';
        this.emailAddress = '';
        this.addressLine1 = '';
        this.addressLine2 = '';
        this.city = '';
        this.postalCode = '';
        this.deviceUsed = '';
        this.companyTypeId = '';
        this.isAdmin = false;
    }
    id: string;
    companyName: string;
    companyRegNo: string;
    firstName: string;
    lastName: string;
    workNumber: string;
    cellNumber: string;
    emailAddress: string;
    addressLine1: string;
    addressLine2: string;
    city: string;
    postalCode: string;
    deviceUsed: string;
    companyTypeId: string;
    isAdmin: boolean;
}
