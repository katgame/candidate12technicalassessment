
export class VehicleDetails {

    constructor() {
        this.incidentYear = '';
        this.vehicleSumInsured = '';
        this.vehicleModel = '';
        this.vehicleMake = '';
        this.vehicleYear = '';
        this.vehicleMass = '';
        this.region = '';
        this.repairCost = '';
    }
    incidentYear: string;
    vehicleSumInsured: string;
    vehicleModel: string;
    vehicleMake: string;
    vehicleYear: string;
    vehicleMass: string;
    region: string;
    repairCost: string;
}
