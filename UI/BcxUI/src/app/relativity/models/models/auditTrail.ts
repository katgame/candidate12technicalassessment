
// tslint:disable-next-line:class-name
export class AuditTrail {

    constructor() {
        this.ClaimNumer = '';
        this.IncidentYear = '';
        this.VehicleSumInsured = '';
        this.VehicleMake = '';
        this.VehicleModel = '';
        this.VehicleYear = '';
        this.VehicleMass = '';
        this.Area = '';
        this.RepairCost = '';
        this.AdditionalCosts = '';
        this.TotalCostRepair = '';
        this.ExpectedSalvageRecovery = '';
        this.CostOfSalvage = '';
        this.TotalSalvageRecovery = '';
        this.DifferenceInCost = '';
        this.Recommendation = '';
        this.Comments = '';
        this.PolicyNumber = '';
        this.Assessor = '';
        this.CreatedDate = new Date;
    }
    ClaimNumer = '';
    IncidentYear = '';
    VehicleSumInsured = '';
    VehicleMake = '';
    VehicleModel = '';
    VehicleYear = '';
    VehicleMass = '';
    Area = '';
    Assessor = '';
    RepairCost = '';
    AdditionalCosts = '';
    TotalCostRepair = '';
    ExpectedSalvageRecovery = '';
    CostOfSalvage = '';
    TotalSalvageRecovery = '';
    DifferenceInCost = '';
    Recommendation = '';
    Comments = '';
    PolicyNumber = '';
    CreatedDate: Date;
}
