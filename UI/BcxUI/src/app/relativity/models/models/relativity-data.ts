export interface RelativityData {
  relativityName: string;
  currentValue: string;
  id: string;
  relativityId: string;
  }
