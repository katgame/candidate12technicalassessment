import { relativityLookUp } from './relativitylookup';

// tslint:disable-next-line:class-name
export class UpdateRelativityInDTO {

    constructor() {
        this.userId = '';
        this.relativity = new relativityLookUp();
        this.reason = '';
    }
    userId: string;
    relativity: relativityLookUp;
    reason: string;
}
