import { VehicleDetails } from './vehicle-details.model';

export class ClaimCalculateDTO {

    constructor() {
        this.policyNumber = '';
        this.assessorId = '';
        this.vehicleDetail = new VehicleDetails();

    }
    policyNumber: string;
    assessorId: string;
    vehicleDetail: VehicleDetails;
    claimNumber: string;

}
