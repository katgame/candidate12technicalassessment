export interface DropDownValues {
  value: string;
  viewValue: string;
}
