import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { SidebarLayoutComponent } from './layouts/sidebar-layout/sidebar-layout.component';

import {
  AuthModule,
  OidcSecurityService,
  ConfigResult,
  OidcConfigService,
  OpenIdConfiguration
} from 'angular-auth-oidc-client';

import { AutoLoginComponent } from './auto-login/auto-login.component';
import { routing } from './app.routes';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { ProtectedComponent } from './protected/protected.component';
import { AuthorizationGuard } from './authorization.guard';
import { environment } from '../environments/environment';
import { ServicesModule } from './services/services.module';
import { UserRegistrationComponent} from './user/user-registration/user-registration.component';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './shared/material-module';
import { HttpModule } from '@angular/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HeaderInterceptor } from './services/head-interceptor';
import { ErrorDialogService } from './services/error-dialog.service';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './store';
import { DialogBoxComponent } from './dialogs/default-dialog/dialog-box.component'; 
import { ErrorHandlerComponent } from './shared/error-handler/error-handler.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { LoginComponent } from './auth/login/login.component';
import { StateModule } from 'src/state/state.module';
import { AuthGuardService } from './services/auth-guard.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ViewUserComponent } from './user/view/view-user.component';

@NgModule({
  declarations: [
    SidebarLayoutComponent,
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    AutoLoginComponent,
    ForbiddenComponent,
    UnauthorizedComponent,
    ProtectedComponent,
    ViewUserComponent,
    UserRegistrationComponent,
    ErrorHandlerComponent,
    LoginComponent,
    DialogBoxComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    AuthModule.forRoot(),
    FormsModule,
    routing,
    ServicesModule,
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MaterialModule,
    HttpModule,
    HttpClientModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    StateModule.forRoot(),
    NgxSpinnerModule
  ],
  entryComponents: [ DialogBoxComponent, ErrorHandlerComponent],
  providers: [
    OidcSecurityService,
    ErrorDialogService,
    OidcConfigService,
    { provide: HTTP_INTERCEPTORS, useClass: HeaderInterceptor, multi: true },
    AuthorizationGuard
  ],
  bootstrap: [AppComponent]
})

export class AppModule {}
