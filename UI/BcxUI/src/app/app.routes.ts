import { Routes, RouterModule } from '@angular/router';

import { ForbiddenComponent } from './forbidden/forbidden.component';
import { HomeComponent } from './home/home.component';
import { AutoLoginComponent } from './auto-login/auto-login.component';
import { ProtectedComponent } from './protected/protected.component';
import { AuthorizationGuard } from './authorization.guard';
import { LoginComponent } from './auth/login/login.component';
import { SidebarLayoutComponent } from './layouts/sidebar-layout/sidebar-layout.component';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';
import { UserRegistrationComponent } from './user/user-registration/user-registration.component';
import {
  AuthGuardService as AuthGuard
} from './services/auth-guard.service';
import { ViewUserComponent } from './user/view/view-user.component';

const appRoutes: Routes = [
  { path: '', component: LoginComponent, pathMatch: 'full'},
  { path: 'home', component: LoginComponent },
  { path: 'autologin', component: AutoLoginComponent },
  { path: 'forbidden', component: ForbiddenComponent },
  { path: 'unauthorized', component: UnauthorizedComponent },
  { path: 'userRegistration', component: UserRegistrationComponent },
  {
    path: '',
    component: SidebarLayoutComponent,
    children : [
      { path: 'view', component: ViewUserComponent , canActivate: [AuthGuard] },
      { path: 'protected', component: ProtectedComponent, canActivate: [AuthorizationGuard] }
    ]
  }
];

export const routing = RouterModule.forRoot(appRoutes);
