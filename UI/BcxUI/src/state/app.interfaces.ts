import { RouterReducerState } from '@ngrx/router-store';
import * as fromSnackbar from './shared/reducers/snackbar.reducer';
import * as fromSpinner from './shared/reducers/spinner.reducer';
import { RouterStateUrl } from "./shared/utils";

export interface AppState {
  router: RouterReducerState<RouterStateUrl>;
  snackbar: fromSnackbar.State;
  spinner: fromSpinner.State;
}
 