import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { map, delay} from "rxjs/operators";
import * as SnackbarActions from '../actions/snackbar.action';

@Injectable()
export class SnackbarEffects {
    constructor(private actions: Actions,
        private matSnackBar: MatSnackBar) {
    }

    @Effect({dispatch: false})
    notification = this.actions.pipe(
        ofType<SnackbarActions.SnackbarOpen>(SnackbarActions.SNACKBAR_OPEN),
        map(({ payload}) => {
            this.matSnackBar.open(payload.message, payload.action, payload.config);
        })
    )}